package com.novykov.starbucks.coffee;

public class Latte extends Espresso {

    private static final int GRAIN_CONSUMPTION = 10;

    private static final int WATER_CONSUMPTION = 35;

    private static final int MILK_CONSUMPTION = 70;

    public Latte() {
        super(GRAIN_CONSUMPTION, WATER_CONSUMPTION, MILK_CONSUMPTION);
    }

    @Override
    public void create() {
        System.out.println("Latte created");
    }
}
