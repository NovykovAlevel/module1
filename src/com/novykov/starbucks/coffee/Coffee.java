package com.novykov.starbucks.coffee;

public abstract class Coffee {

    private int grainConsumption;

    private int waterConsumption;

    private int milkConsumption;

    Coffee(int grainConsumption, int waterConsumption, int milkConsumption) {
        this.grainConsumption = grainConsumption;
        this.waterConsumption = waterConsumption;
        this.milkConsumption = milkConsumption;
    }

    public int getGrainConsumption() {
        return grainConsumption;
    }

    public int getWaterConsumption() {
        return waterConsumption;
    }

    public int getMilkConsumption() {
        return milkConsumption;
    }

    public abstract void create();
}
