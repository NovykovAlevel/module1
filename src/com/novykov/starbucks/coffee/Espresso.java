package com.novykov.starbucks.coffee;

public class Espresso extends Coffee {

    private static final int GRAIN_CONSUMPTION = 10;

    private static final int WATER_CONSUMPTION = 35;

    public Espresso() {
        super(GRAIN_CONSUMPTION, WATER_CONSUMPTION, 0);
    }

    public Espresso(int cornConsumption, int waterConsumption, int milkConsumption) {
        super(cornConsumption, waterConsumption, milkConsumption);
    }

    @Override
    public void create() {
        System.out.println("Espresso created");
    }
}
