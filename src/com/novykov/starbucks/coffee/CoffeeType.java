package com.novykov.starbucks.coffee;

public enum CoffeeType {
    ESPRESSO, AMERICANO, LATTE
}
