package com.novykov.starbucks.coffee;

public class Americano extends Espresso {

    private static final int GRAIN_CONSUMPTION = 16;

    private static final int WATER_CONSUMPTION = 75;

    public Americano() {
        super(GRAIN_CONSUMPTION, WATER_CONSUMPTION, 0);
    }

    @Override
    public void create() {
        System.out.println("Americano created");
    }
}
