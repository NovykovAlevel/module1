package com.novykov.starbucks.client;

import java.util.ArrayList;

public class ClientPref {

    public static final int CLIENTS_COUNT = 10;

    private ArrayList<String> names = new ArrayList<>(CLIENTS_COUNT);

    public ClientPref() {
        names.add("Sasha");
        names.add("Igor");
        names.add("Petya");
        names.add("Masha");
        names.add("Ira");
        names.add("Valera");
        names.add("Maxim");
        names.add("Alina");
        names.add("Dima");
        names.add("Vitalik");
    }

    public ArrayList<String> getNames() {
        return names;
    }
}
