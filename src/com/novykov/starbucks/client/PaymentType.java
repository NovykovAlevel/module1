package com.novykov.starbucks.client;

public enum PaymentType {
    CARD, CASH
}
