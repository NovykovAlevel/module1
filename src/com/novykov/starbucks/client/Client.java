package com.novykov.starbucks.client;

public class Client {

    private String name;

    private int cashBalance;

    private int cardBalance;

    public Client(String name, int cashBalance, int cardBalance) {
        this.name = name;
        this.cashBalance = cashBalance;
        this.cardBalance = cardBalance;
    }

    public String getName() {
        return name;
    }

    public int getCashBalance() {
        return cashBalance;
    }

    public int getCardBalance() {
        return cardBalance;
    }

    public PaymentType getPaymentType(int coffeePrice) {
        if (isCashBalanceEnough(coffeePrice)) {
            System.out.println("Payment type - Cash");
            return PaymentType.CASH;
        } else if (isCardBalanceEnough(coffeePrice)) {
            System.out.println("Payment type - Card");
            return PaymentType.CARD;
        }
        System.out.println("Payment type - Unknown");
        return null;
    }

    public void isPaySuccess(PaymentType type, int coffeePrice) {
        switch (type) {
            case CASH:
                cashBalance = cashBalance - coffeePrice;
                System.out.println("Payment successfully made. Cash balance: " + cashBalance);
                break;

            case CARD:
                cardBalance = cardBalance - coffeePrice;
                System.out.println("Payment successfully made. Card balance: " + cardBalance);
                break;
        }
    }

    private boolean isCashBalanceEnough(int coffeePrice) {
        return cashBalance > coffeePrice;
    }

    private boolean isCardBalanceEnough(int coffeePrice) {
        return cardBalance > coffeePrice;
    }
}
