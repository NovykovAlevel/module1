package com.novykov.starbucks.staff;

import com.novykov.starbucks.coffee.CoffeeType;
import com.novykov.starbucks.coffee.Coffee;
import com.novykov.starbucks.warehouse.Warehouse;

public class CoffeeMaker {

    private static final int ESPRESSO_PRICE = 10;
    private static final int AMERICANO_PRICE = 20;
    private static final int LATTE_PRICE = 25;

    private Warehouse warehouse;

    public CoffeeMaker(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    void createCoffee(Coffee coffee) {
        checkIngredientsEnough(coffee);
        coffee.create();
        warehouse.coffeeCreated(coffee);
    }

    private void checkIngredientsEnough(Coffee coffee) {
        if (!warehouse.isCupsEnough()) {
            System.out.println("Warehouse. Not enough cups. In warehouse "
                    + warehouse.getCupsAmount() + ". Need 1. Refresh cups");
            warehouse.refreshCups();
        }

        if (!warehouse.isGrainEnough(coffee.getGrainConsumption())) {
            System.out.println("Warehouse. Not enough grains. In warehouse "
                    + warehouse.getGrainAmount() + ". Need " + coffee.getGrainConsumption() + ". Refresh grains");
            warehouse.refreshGrains();
        }

        if (!warehouse.isMilkEnough(coffee.getMilkConsumption())) {
            System.out.println("Warehouse. Not enough milk. In warehouse "
                    + warehouse.getMilkAmount() + ". Need " + coffee.getMilkConsumption() + " .Refresh milk");
            warehouse.refreshMilk();
        }

        if (!warehouse.isWaterEnough(coffee.getWaterConsumption())) {
            System.out.println("Warehouse. Not enough water. In warehouse "
                    + warehouse.getWaterAmount() + ". Need " + coffee.getWaterConsumption() + ". Refresh water");
            warehouse.refreshWater();
        }
    }

    public int getCoffeePrice(CoffeeType type) {
        switch (type) {
            case ESPRESSO:
                return ESPRESSO_PRICE;

            case AMERICANO:
                return AMERICANO_PRICE;

            case LATTE:
                return LATTE_PRICE;

            default:
                return 0;
        }
    }
}
