package com.novykov.starbucks.staff;

import java.util.Random;

import com.novykov.starbucks.coffee.Coffee;
import com.novykov.starbucks.warehouse.Warehouse;

public class CoffeeMachine extends CoffeeMaker {

    public CoffeeMachine(Warehouse warehouse) {
        super(warehouse);
    }

    public boolean isWork() {
        return new Random().nextBoolean();
    }

    public void create(Coffee coffee) {
        System.out.println("Coffee created by machine");
        createCoffee(coffee);
    }
}
