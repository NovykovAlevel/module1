package com.novykov.starbucks.staff;

import com.novykov.starbucks.coffee.Coffee;
import com.novykov.starbucks.warehouse.Warehouse;

public class Barista extends CoffeeMaker {

    private CoffeeMachine coffeeMachine;

    public Barista(Warehouse warehouse) {
        super(warehouse);
        this.coffeeMachine = new CoffeeMachine(warehouse);
    }

    public void create(Coffee coffee) {
        if (coffeeMachine.isWork()) {
            coffeeMachine.create(coffee);
        } else {
            System.out.println("Coffee machine not work. Created by barista");
            createCoffee(coffee);
        }
    }
}
