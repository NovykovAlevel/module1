package com.novykov.starbucks.warehouse;

import com.novykov.starbucks.coffee.Coffee;

public class Warehouse {

    public static final int DEFAULT_CUPS = 10;
    public static final int DEFAULT_GRAINS = 100;
    public static final int DEFAULT_WATER = 500;
    public static final int DEFAULT_MILK = 50;

    private int grainAmount;

    private int waterAmount;

    private int milkAmount;

    private int cupsAmount;

    public Warehouse(int grainAmount, int waterAmount, int milkAmount, int cupsAmount) {
        this.grainAmount = grainAmount;
        this.waterAmount = waterAmount;
        this.milkAmount = milkAmount;
        this.cupsAmount = cupsAmount;
    }

    public boolean isGrainEnough(int count) {
        return grainAmount - count > 0;
    }

    public boolean isWaterEnough(int count) {
        return waterAmount - count > 0;
    }

    public boolean isMilkEnough(int count) {
        return milkAmount - count > 0;
    }

    public boolean isCupsEnough() {
        return cupsAmount > 0;
    }

    public int getGrainAmount() {
        return grainAmount;
    }

    public int getWaterAmount() {
        return waterAmount;
    }

    public int getMilkAmount() {
        return milkAmount;
    }

    public int getCupsAmount() {
        return cupsAmount;
    }

    public void refreshCups() {
        cupsAmount = DEFAULT_CUPS;
    }

    public void refreshGrains() {
        grainAmount = DEFAULT_GRAINS;
    }

    public void refreshWater() {
        waterAmount = DEFAULT_WATER;
    }

    public void refreshMilk() {
        milkAmount = DEFAULT_MILK;
    }

    public void coffeeCreated(Coffee coffee) {
        grainAmount = grainAmount - coffee.getGrainConsumption();
        waterAmount = waterAmount - coffee.getWaterConsumption();
        milkAmount = milkAmount - coffee.getMilkConsumption();
        cupsAmount--;
    }
}
