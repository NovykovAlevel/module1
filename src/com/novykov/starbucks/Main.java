package com.novykov.starbucks;

import java.util.ArrayList;
import java.util.Random;

import com.novykov.starbucks.bank.Bank;
import com.novykov.starbucks.client.Client;
import com.novykov.starbucks.client.ClientPref;
import com.novykov.starbucks.client.PaymentType;
import com.novykov.starbucks.coffee.Americano;
import com.novykov.starbucks.coffee.Coffee;
import com.novykov.starbucks.coffee.CoffeeType;
import com.novykov.starbucks.coffee.Espresso;
import com.novykov.starbucks.coffee.Latte;
import com.novykov.starbucks.staff.Barista;
import com.novykov.starbucks.warehouse.Warehouse;

public class Main {
    public static void main(String[] args){

        ClientPref pref = new ClientPref();
        ArrayList<Client> clients = new ArrayList<>(ClientPref.CLIENTS_COUNT);
        for (int i = 0; i < ClientPref.CLIENTS_COUNT; i++) {
            Client client = new Client(
                    pref.getNames().get(i),
                    new Random().nextInt(100),
                    new Random().nextInt(500));
            clients.add(client);
        }

        Warehouse warehouse = new Warehouse(
                new Random().nextInt(Warehouse.DEFAULT_GRAINS),
                new Random().nextInt(Warehouse.DEFAULT_WATER),
                new Random().nextInt(Warehouse.DEFAULT_MILK),
                new Random().nextInt(Warehouse.DEFAULT_CUPS));

        Bank bank = new Bank();

        Barista barista = new Barista(warehouse);

        for (Client client : clients) {
            System.out.println("Client "
                    + client.getName()
                    + " with cash balance: "
                    + client.getCashBalance()
                    + " | card balance: "
                    + client.getCardBalance()
                    + " want to buy coffee");
            CoffeeType type = getCoffeeType();
            int price = barista.getCoffeePrice(type);
            System.out.println("Coffee price " + price);
            PaymentType paymentType = client.getPaymentType(price);
            if (paymentType != null) {
                if (paymentType == PaymentType.CARD && !bank.isWorks()) {
                    System.out.println("Bank not work. Sorry" + "\n **********");
                } else {
                    barista.create(getCoffeeByType(type));
                    client.isPaySuccess(paymentType, price);
                    System.out.println("Client received coffee with a wish on the cup: Have a nice day "
                            + client.getName() + "\n **********");
                }
            }
        }
    }

    private static CoffeeType getCoffeeType() {
        int type = new Random().nextInt(2);
        switch (type) {
            case 0:
                System.out.println("Coffee type Espresso");
                return CoffeeType.ESPRESSO;

            case 1:
                System.out.println("Coffee type Americano");
                return CoffeeType.AMERICANO;

            case 2:
                System.out.println("Coffee type Latte");
                return CoffeeType.LATTE;

            default:
                System.out.println("Coffee type Espresso");
                return CoffeeType.ESPRESSO;
        }
    }

    private static Coffee getCoffeeByType(CoffeeType type) {
        switch (type) {
            case ESPRESSO:
                return new Espresso();

            case AMERICANO:
                return new Americano();

            case LATTE:
                return new Latte();

            default:
                return new Espresso();
        }
    }
}
